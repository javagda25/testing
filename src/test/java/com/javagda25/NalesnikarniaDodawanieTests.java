package com.javagda25;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

@RunWith(JUnit4.class)
public class NalesnikarniaDodawanieTests {
    private static Nalesnikarnia nalesnikarnia = new Nalesnikarnia();

    @Test
    public void test_czy_dodaje_nalesniki() {
        nalesnikarnia.stworzNalesnika(Arrays.asList("chleb", "masło"));
        nalesnikarnia.stworzNalesnika(Arrays.asList("pierniki", "dzem"));

        Assert.assertEquals(2, nalesnikarnia.pobierzIloscGotowychNalesnikow());
    }
}
