package com.javagda25;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

import java.util.Arrays;

@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NalesnikarniaPobieranieTests {
    private Nalesnikarnia nalesnikarnia = new Nalesnikarnia();

    @Before
    public void przygotujNalesnikarnie() {
        nalesnikarnia.wyczyscWszystkieNalesniki();
        nalesnikarnia.stworzNalesnika(Arrays.asList("chleb", "masło"));
        nalesnikarnia.stworzNalesnika(Arrays.asList("pierniki", "dzem"));
    }

    @Test
    public void test_1_czy_moge_pobrac_nalesniki() {
        Nalesnik n1 = nalesnikarnia.pobierzNalesnikaZeStosu();
        Nalesnik n2 = nalesnikarnia.pobierzNalesnikaZeStosu();

        Assert.assertEquals(0, nalesnikarnia.pobierzIloscGotowychNalesnikow());
    }


    @Test
    public void test_2_czy_moge_pobrac_nalesniki() {
        Nalesnik n1 = nalesnikarnia.pobierzNalesnikaZeStosu();

        Assert.assertTrue(n1.getSkladniki().containsAll(Arrays.asList("pierniki", "dzem")));
    }

    @Test
    public void test_3_czy_moge_pobrac_nalesniki() {
        Nalesnik n1 = nalesnikarnia.pobierzNalesnikaZeStosu();
        n1 = nalesnikarnia.pobierzNalesnikaZeStosu();

        Assert.assertTrue(n1.getSkladniki().containsAll(Arrays.asList("chleb", "masło")));
    }

    @Test(expected = NoPancakeException.class)
    public void test_4_czy_moge_pobrac_nalesniki() {
        Nalesnik n1 = nalesnikarnia.pobierzNalesnikaZeStosu();
        n1 = nalesnikarnia.pobierzNalesnikaZeStosu();
        n1 = nalesnikarnia.pobierzNalesnikaZeStosu();
    }
}
