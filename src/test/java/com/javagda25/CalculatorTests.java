package com.javagda25;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CalculatorTests {

    private static Calculator calculator;

    @BeforeClass
    public static void funkcjaWywolanaRazPrzedWszystkimiTestami(){
        System.out.println("Rozpoczynam testy w klasie: " + CalculatorTests.class);
        calculator = new Calculator();
    }

    @Before
    public void funkcjaPrzedKazdymTestem(){
        System.out.println("Testowanko");
    }

    @Test
    public void calculator_Should_Return_10_When_Adding_5_And_5() {
        int sumResult = calculator.sum(5, 5);

        // zapewniam, że wynik (sumowania) powinien być 10
        Assert.assertEquals(10, sumResult);
    }

    @Test
    public void calculator_Should_Return_10_When_Adding_6_And_4() {
        int sumResult = calculator.sum(6, 4);

        // zapewniam, że wynik (sumowania) powinien być 10
        Assert.assertEquals(10, sumResult);
    }
}
