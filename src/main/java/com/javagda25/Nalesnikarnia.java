package com.javagda25;


import java.util.List;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Nalesnikarnia {

    private Stack<Nalesnik> nalesniks = new Stack<>();

    public Nalesnikarnia() {
    }

    public void stworzNalesnika(List<String> asList) {
        System.out.println("Tworzymy nowy naleśnik.");
        nalesniks.push(new Nalesnik(asList));
    }

    public Nalesnik pobierzNalesnikaZeStosu() {
        if (nalesniks.empty()) {
            throw new NoPancakeException();
        }
        Nalesnik zabrany = nalesniks.pop();
        return zabrany;
    }

    public int pobierzIloscGotowychNalesnikow() {
        return nalesniks.size();
    }

    public void wyczyscWszystkieNalesniki() {
        nalesniks.clear();
    }
}
